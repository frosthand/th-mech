unit mech;
interface
uses GDI5,DI5w,graphics,fnmath;
Type
  mas = Array[0..360] of double;

  Tmechanism=object

  T2,T3,T4,T5,Tsum2,vex,J2,J3,J4,J5,Jprsum: mas;
  O,A,B,C,D,E,L,G:para;
  OA,AB,BC,CD,DE,EL,LG:zveno;
  mpr,mprs,Asum,om,mfs,mpr_sd                    : mas;
  mprd,omegasr,deltar,deltai,DelT1        :double;

   m2,m3,m4,m5,Is2,Is3,Is4,Is5,IConst,Imaxmas :double;
   fs            : double;
   fiAB          : double;
   lec,vec,aec               : double;
   s2x,s2y,s3x,s3y,s4x,s4y                  :double;
   s2xi,s2yi,s3xi,s3yi,s4xi,s4yi            :integer;

   damax,damin    :double;
   sum,rab,t0     :double;
   vs2x,vs2y,vs3x,vs3y,vs4x,vs4y,vs5x,vs5y: double;
   mechGraph:  TmechGraph;

   mech:Tmech;
    Procedure InitParam;
    Procedure InitMech;
    Procedure Result;
    Procedure DrawMech(i:integer);
    Procedure Mech_1(fi:double);
end;

function mech_Ksum(i:integer):double;
function mech_SumPrivMoment(i:integer):double;
function mech_mprd(i:integer):double;
const
piNa180=3.141592/180;
var
mechanism: Tmechanism;
implementation

function mech_Ksum(i:integer):double ;
begin
  mech_Ksum:=mechanism.asum[i]-mechanism.tsum2[i];
end;
function mech_SumPrivMoment(i:integer):double;
begin
  mech_SumPrivMoment:=mechanism.mprs[i]+mechanism.mprd;
end;
function mech_mprd(i:integer):double;
begin
  mech_mprd  :=mechanism.mprd;
end;
function mech_AverageVelocity(i:integer):double;
begin
  mech_AverageVelocity:=0;
end;

Procedure Tmechanism.InitParam;   {���� �������� � ����}
   begin
                 {����� (�)}
    DE.l:=1.1;
    CD.l:=DE.l/1.5;
    EL.l:=0.3*DE.l;
    AB.l:=0.287;
    BC.l:=1.098;


    D.x := -1.05; D.y := -0.36; C.x := 0; C.y := 0;
    G.x:=-2;
    G.y:=d.y+1.05;

    {����� � ������� ������� (�� � ��*�*�)}

    m2:=42;m3 := 150; m4 := 28;m5 := 28;
    Iconst := 3.5; {�������, ��������� ��������}
    fs := 1000;    { �������� ���� ��������� ������������� (�)}

    AB.om:=1;
    omegasr := 15; {�������� ������� �������� ���������� ����� (���/���)}
    deltai:=30;     { ���������������}
    deltar := 1/deltai;{1/30}


    mech.AddPointFixed(0,0,'A');
    mech.AddPointVariable(0,'B');
    mech.AddPointVariable(0,'C');
    mech.AddPointFixed(-1.05,-0.36,'D');
    mech.AddPointVariable(0,'E');
    mech.AddPointVariable(0,'L');
    mech.AddPointFixed(G.x,G.y,'G');
    mech.AddPointFixed(-1.25,-0.66,'D1');
    mech.AddPointVariable(0,'C1');
    mech.AddPointFixed(-2,0,'G1');
    mech.AddPointVariable(0,'E1');
    mech.AddPointVariable(0,'L1');

    mech.AddBlock('A','B',AB.l,0,0,jointTypeR,jointTypeR,'1');
    mech.AddBlock('B','C',BC.l,m2,BC.l*0.5,jointTypeR,jointTypeR,'2');
    mech.AddPointToBlock( 'E1',BC.l*0.5,BC.l*0.25,1,0,jointTypeR,'2');
    mech.AddBlock('D','C',CD.l,0,0,jointTypeR,jointTypeR,'3');
    mech.AddPointToBlock('E',DE.l,DE.l*0.5,m3,0,jointTypeR,'3');
    mech.AddBlock('E','L',EL.l,m4,EL.l*0.5,jointTypeR,jointTypeR,'4');
    mech.AddBlock('G','L',0,m5,0,jointTypeSeal,jointTypeP,'5');

    mech.AddBlock('E1','L1',CD.l,1,CD.l*0.5,jointTypeR,jointTypeR,'41');
    mech.AddBlock('G1','L1',0,1,0,jointTypeSeal,jointTypeP,'51');

    mech.AddBlock( 'D1','C1',CD.l*0.2,15,CD.l*0.2*0.7,jointTypeR,jointTypeR,'31');
    mech.AddBlock('B','C1',CD.l*1.9,15,CD.l*1.9*0.3,jointTypeR,jointTypeR,'21');



    mech.InitGraph(mechGraph);
   End;
  Procedure Tmechanism.Mech_1(fi:double);  {������ �������� ��� ������ ��������� }
                    {�������������� ��������� ��������� ���������� rab}
   Begin
    mech.Run('31',fi,1,0);

    BC:=mech.GetZveno('2');
    DE:=mech.GetZveno('3');
    EL:=mech.GetZveno('4');
    B:=mech.GetPara('B');
    C:=mech.GetPara('C');

    E:=mech.GetPara('E');
    L:=mech.GetPara('L');

    vs2x := (B.vx+C.vx)*0.5;
    vs2y := (B.vy+C.vy)*0.5;
    vs3x := E.vx*0.5;
    vs3y := E.vy*0.5;
    vs4y := (E.vy+L.vy)*0.5;
    vs4x := (E.vx+L.vx)*0.5;
    vs5x := L.vx;
   End;
  Procedure Tmechanism.InitMech; { ������ ���������� ��� ����� ����� � ���������� ��������}
   Var i,j: integer;
   lxmin,lxmax:double; {��� ����������� ������� �������� ������� ����}
   Begin
   lxmin:=100;
   lxmax:=-100;
  fiAB:= 0;
  
  for i := 0 to 359 do   {������ ��������� � ���������� ��������}
   begin { for i }
    fiAB := fiAB+ pina180;
    Mech_1(fiAB);
            {������� ������������ ������������ ��������� ������� ����}
                   { ������ �������� ��������� �����}
    {vs5x := vec;}
    vec:=vs5x;
    vex[i]:= vec;
    J2[i]:=mech.blocks[mech.FindBlock('2')].Jp;
    J3[i]:=mech.blocks[mech.FindBlock('3')].Jp;
    J4[i]:=mech.blocks[mech.FindBlock('4')].Jp;
    J5[i]:=mech.blocks[mech.FindBlock('5')].Jp;
   {����������� ������� ������� � ������������ ������� ������ ������ �������}
    T2[i] := mech.blocks[mech.FindBlock('2')].Jp* sqr(omegasr)/ 2;
    T3[i] := mech.blocks[mech.FindBlock('3')].Jp* sqr(omegasr)/ 2;
    T4[i] := mech.blocks[mech.FindBlock('4')].Jp* sqr(omegasr)/ 2;
    T5[i] := mech.blocks[mech.FindBlock('5')].Jp* sqr(omegasr)/ 2;
   {��������� ����������� ������ ������� � �������������� ������� �������}
    Jprsum[i]:=J2[i] + J3[i]+ J4[i]+ J5[i];
    Tsum2[i] :=T2[i] + T3[i]+ T4[i]+ t5[i];
    if (lxmin>L.x) then lxmin:=L.x;
    if (lxmax<L.x) then lxmax:=L.x;

   end; {i}
   fiAB:=0;
   sum:=0;
   for i:=0 to 359 do
   begin
    j:=i-1;
    fiAB := fiAB+ pina180;
    Mech_1(fiAB);
     if (lxmax-L.x)<=(lxmax-lxmin)*0.2 then  {������ ��������� ����������� ��������}
      begin
          mfs[i]:=fs*0.10;
      end
      else
      begin
          mfs[i]:= - fs;
      end;
      {����������� �������}
     mpr[i] := mfs[i]* vs5x;         { �� ���� ��������� �������������}
     mprs[i] := -m2*9.81*vs2y - m3* 9.81* vs3y- m4* 9.81* vs4y+ mpr[i];{ ��������� �� ��� ���� � �������������}

     sum := sum + mpr[i];{�������� �� ������� ���� ��������� �������������}
   end;
   mprd := - sum/360;
   {InitForce;}
  End;

  Procedure Tmechanism.Result;     {���������� �������� ������������ �������������}
   Var i:integer;
       ss:String[20];
   Begin
    sum := 0;
    asum[0] := 0;
    damax := 0; damin := 100;
    for i := 0 to 359 do                  { ������}
     begin
      sum := sum + ((mprs[i - 1] + mprs[i])/2 + mprd)*pina180;
      asum[i] := sum;   { ������ ���������� ����������� ������� }
         { ������������ ������� ������ ������}
      rab:= asum[i] - Tsum2[i];
      om[i]:= rab;                {�����}
       if damax <  rab then
        damax := rab;
       if damin > rab then
        damin := rab;
     end;
    asum[0]:= asum[360];
    DelT1:= damax- damin;   {������� T1}{ �������}
    Imaxmas := DelT1/ Sqr(omegasr)/ deltar;
                                  { ������� ��������}
    rab:= omegasr* deltar;       {omega max - omega min}
    rab:= rab/ DelT1;

    t0:= omegasr- rab* (damax+ damin)/ 2;

    for i := 0 to 359 do
     begin
      om[i] := om[i]* rab+ t0;
     end;
   End;

  Procedure Tmechanism.DrawMech(i:integer);
   Begin
    mech.Run('31',i * pina180,1,0);
    mech.Draw;
   End;{draw}
end.
