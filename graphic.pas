unit graphic;

interface
uses Windows,Graphics,fnmath;
const valueWidth=80;{������ ������� ��� ��������}
offsetTop=18;{}
offsetBottom=18;
offsetRight=20;
offsetLeft=80;

type
TValueFn=function(i:integer):double;
Tgraphic=object
  data:Pdouble;
  GenericFunction:PBaseFn;
  name:String;
  color:Tcolor;
  scaleY:double;
  valueFn:TValueFn;
  useFunction:boolean;
  useGenericFn:boolean;
  Constructor Init;
  function Value(i:integer):double;
end;
TgraphicView=object
  output:TCanvas;
  graphics: array[0..10] of Tgraphic;
  numGraph:integer;
  scaleX,scaleY:double;
  sizeX,sizeY:integer;
  posX,posY:integer;
  centerX,centerY:integer;
  Constructor Init(out:Tcanvas);

  Procedure SetSize(x,y:integer);
  Procedure SetPos(x,y:integer);
  Procedure AddGraphByData(data:Pdouble;name:string;color:Tcolor;scale:double);
  Procedure AddGraphByFn(valueFn:TValueFn;name:string;color:Tcolor;scale:double);
  Procedure AddGraphByGeneric(fn:PBaseFn;name:string;color:TColor;scale:double);
  Procedure DrawSystem;
  Procedure DrawValue(i:integer);
  Function Count:integer;
  Function Max(g:integer): double;
  Function Min(g:integer): double;

private
  Procedure Draw(g:integer);
  Procedure Bar(x1,y1,x2,y2:integer);
  Procedure line(x1,y1,x2,y2:integer);
  Procedure OutTextXY(x,y:integer; text:String);
  Procedure SetColor(color:integer);
  Procedure Axes;
end;

implementation
Constructor Tgraphic.Init ;
begin
  data:=nil;
end;
Function Tgraphic.Value(i:integer):double;
var
pdata:Pdouble;
begin
  if useGenericFn then Value:=GenericFunction.value(i)
  else
  if useFunction then Value:=valueFn(i)
  else begin
    pdata:=data;
    if pdata=nil then Value:=0
    else begin
    inc(pdata,i);
    Value:=pdata^;
    end;
  end;
end;
Constructor TgraphicView.Init(out:Tcanvas);
begin
  output:=out;
  scaleX:=1;
  scaleY:=1;
end;
Function TgraphicView.Count:integer;
begin
  Count:=numGraph;
end;
Function TgraphicView.Max(g:integer):double;
var
  nmax:double;
  i:integer;
begin
  nmax:=graphics[g].Value(0);
  for i:=1 to 359 do
  begin
  if graphics[g].Value(i)*graphics[g].scaleY> nmax then nmax:=graphics[g].Value(i)*graphics[g].scaleY;
  end;
  Max:=nmax;
end;
Function TgraphicView.Min(g:integer):double;
var
  nmin:double;
  i:integer;
begin
  nmin:=graphics[g].Value(0);
  for i:=1 to 359 do
  begin
  if graphics[g].Value(i)*graphics[g].scaleY< nmin then nmin:=graphics[g].Value(i)*graphics[g].scaleY;
  end;
  Min:=nmin;
end;
Procedure TgraphicView.Draw(g:integer);
var
i,j:integer;
begin
  for i := 0 to 359  do
  begin
     j := i + 1;
     output.MoveTo(centerX+round(i*scaleX),centerY-round(graphics[g].Value(i)*scaleY*graphics[g].scaleY));
     output.LineTo(centerX+round(j*scaleX),centerY-round(graphics[g].Value(j)*scaleY*graphics[g].scaleY));
  end;
end;
Procedure TgraphicView.SetSize(x,y:integer);
var
maxHeight:double;
mmax,mmin:double;
i:integer;
begin
  {maxHeight:=Max(0)-Min(0);}
  mmax:=Max(0);
  mmin:=Min(0);
  sizeX:=x;
  sizeY:=y;
  for i:=1 to numGraph-1 do
  begin
    if Max(i)>mmax then mmax:=Max(i);
    if Min(i)<mmin then mmin:=Min(i);
  end;
  maxHeight:=mmax-mmin;
  if maxHeight=0 then maxHeight:=1;
  scaleY:=(sizeY-(offsetTop+offsetBottom))/maxHeight;
  scaleX:=(sizeX-(offsetLeft+offsetRight))/360;
  centerY:=15+posY+round(mmax*scaleY);
  centerX:=posX+valueWidth;
end;
Procedure TgraphicView.AddGraphByData(data:Pdouble;name:string;color:Tcolor;scale:double);
begin
  graphics[numGraph].scaleY:=scale;
  graphics[numGraph].data:=data;
  graphics[numGraph].name:=name;
  graphics[numGraph].color:=color;
  graphics[numGraph].useFunction:=false;
  inc(numGraph);
end;
Procedure TgraphicView.AddGraphByFn(valueFn:TValueFn;name:string;color:Tcolor;scale:double);
begin
  graphics[numGraph].scaleY:=scale;
  graphics[numGraph].valueFn:=valueFn;
  graphics[numGraph].name:=name;
  graphics[numGraph].color:=color;
  graphics[numGraph].useFunction:=true;
  inc(numGraph);
end;
Procedure TgraphicView.AddGraphByGeneric(Fn:PBaseFn;name:string;color:Tcolor;scale:double);
begin
  graphics[numGraph].scaleY:=scale;
  graphics[numGraph].GenericFunction:=fn;
  graphics[numGraph].name:=name;
  graphics[numGraph].color:=color;
  graphics[numGraph].useFunction:=false;
  inc(numGraph);
end;
Procedure TgraphicView.DrawSystem ;
var
i:integer;
begin
  Axes;
  output.Font.Size:=8;
  output.Pen.Width:=2;
  for i:=0 to numGraph-1 do
  begin
    SetColor(graphics[i].color);
    outtextXY(posX,posY+i*15,graphics[i].name);
    Draw(i);
  end;
  output.Pen.Width:=1;
end;
Procedure TgraphicView.DrawValue(i:integer) ;
var
j:integer;
tmp:string[50];
begin
  Bar(posX,posY,posX+sizeX,posY+sizeY);
  DrawSystem;
  for j:=0 to numGraph-1 do
  begin
    Str(graphics[j].Value(i):5:1,tmp);
    SetColor(graphics[j].color);
    outtextXY(posX,posY+j*15,graphics[j].name+'='+tmp);
    {Draw(i); }
    SetColor(clBlack);
    line(centerX+round(i*scaleX),posY,centerX+round(i*scaleX),posY+sizeY);
  end;
end;
Procedure TgraphicView.SetPos(x,y:integer);
begin
  posX:=x;
  posY:=y;
end;
Procedure TgraphicView.Bar(x1,y1,x2,y2:integer);
var
  rc:TRect;
begin
  SetRect(rc,x1,y1,x2,y2);
  output.FillRect(rc);
end;
Procedure TgraphicView.SetColor(color:integer);
begin
  output.Pen.Color:=color;
  output.Font.Color:=color;
end;
Procedure TgraphicView.Line(x1,y1,x2,y2:integer);
begin
  output.MoveTo(x1,y1);
  output.LineTo(x2,y2);
end;
Procedure TgraphicView.OutTextXY(x,y:integer;text:String);
begin
  output.TextOut( x,y,text);
end;
Procedure TgraphicView.Axes;
       {����������� ������������ ���� ������ l � ������ x,y }
Var
sconv: String[3];
  i:integer;
Begin
  SetColor(clBlack);
  {Horisontal axis}
  line(centerX,centerY,posX+sizeX,centerY);
  line(posX+sizeX-10,centerY-2,posX+sizeX,centerY);
  line(posX+sizeX-10,centerY+2,posX+sizeX,centerY);
  {dots and labels}
  for i:=0 to 12 do
  begin
    str(30*i,sconv);
    output.Font.Size:=8;
    line(centerX+round(30*i*scaleX),centerY-2,centerX+round(30*i*scaleX),centerY+2);
    outtextxy(centerX+round(30*i*scaleX)-7,centerY+3,sconv);
  end;
  {Vertical axis}
  line(centerX,posY+15,centerX,posY+sizeY-15);
  line(centerX-2,posY+25,centerX,posY+15);
  line(centerX+2,posY+25,centerX,posY+15);
End;
end.
