Unit GDi5;
Interface
 Uses Windows, Messages, SysUtils, Variants, Classes, Graphics;
type
PDouble= ^double;
Pinteger=^integer;
Point5 = Array[1..5] of TPoint;
TmechGraph=object
output:TCanvas; 
PT : Array [1..4] of TPoint;
w,h:integer;
  constructor Init(out:TCanvas;width,height:integer);
  Procedure Axes(x,y,l: integer);
  Procedure Circ(x,y: integer);
  Procedure Opora(x,y: integer; Name: String);
  Procedure Opora90(x,y: integer; Name: String);
  Procedure OporaPost(x,y: integer; FI: Real; Name: String);
  Procedure NamZ(xl,yl,xr,yr: integer; Name: String);
  Procedure InitLink(xst,yst,xa,ya: integer; Name: String);
  Procedure Diada1g(xl,yl,xs,ys,xr,yr: integer; Name: String);
  Procedure PPair(pgx,pgy:integer; FI:Real);
  Procedure PPairE(pgx,pgy:integer; FI:Real);
  Procedure Diada2g(xl,yl,xs,ys,xp,yp: integer; fip: real; Name: String);
  Procedure Diada3g(xa,ya,xb,yb,xc,yc,xd,yd: integer; fi: real; Name: String);
  Procedure Diada4g(xa,ya,xb,yb,xc,yc: integer; fi2,fi3: real; Name: String);
  Procedure Diada5g(xr,yr,xop2,yop2,xop3,yop3,x4,y4,x5,y5: integer;
          fi2,fi3: real; Name:String);
  Procedure Row(x,y: integer; alfa: real);
  Procedure ArrD(px,py:integer);
  Procedure ArrL(px,py:integer);
  Procedure Vect(px,py,vx,vy:integer);
  Procedure Draw(ppg:TPoint; pFI:Real; var PP: Point5);
  Procedure Bar(x1,y1,x2,y2:integer);

  Procedure line(x1,y1,x2,y2:integer);
  Procedure OutTextXY(x,y:integer; text:String);
  Procedure SetColor(color:integer);
end;
 Var cw:byte;
Implementation

Const
     kEGA = 1.0 {��������� ���������� ������������� ����� - VGA};
{VAR
     PT : Array [1..4] of PointType;
}
constructor TmechGraph.Init(out:TCanvas;width,height:integer);
begin
  output:=out;
  output.Font.Height:=5;
  w:=width;
  h:=height;
end;
Procedure Tmechgraph.Bar(x1,y1,x2,y2:integer);
var
  rc:TRect;
begin
    SetRect(rc,x1,y1,x2,y2);
    output.FillRect(rc);
end;
Procedure TmechGraph.SetColor(color:integer);
begin
  output.Pen.Color:=color;
end;
Procedure TmechGraph.line(x1,y1,x2,y2:integer);
begin
  output.MoveTo(x1,y1);
  output.LineTo(x2,y2);
end;
Procedure TmechGraph.OutTextXY(x,y:integer;text:String);
begin
  output.TextOut( x,y,text);
end;
Procedure TmechGraph.Axes(x,y,l: integer);
       {����������� ������������ ���� ������ l � ������ x,y }
Begin
  {SetColor(magenta);
  SetWriteMode(0); }
  line(x - 10,y,x + l,y);
  line(x + l,y,x + l-10,y - 5);
  line(x + l,y,x + l-10,y + 5);

  OutTextXY(x + l+5,y - 7,'X');
  line(x,y + 10,x,y - l);
  line(x,y - l,x - 5,y - l+10);
  line(x,y - l,x + 5,y - l+10);
  OutTextXY(x + 5,y - l,'Y');
  {SetColor(cw);}
End;

Procedure TmechGraph.Circ(x,y: integer);
  {����������� ������������ ���� ( R=3 ) �� ��������� ������ ����������}
 Begin
  {SetFillStyle(1,GetBkColor);}
  {FillEllipse(X,Y,3,3);}
  {SetColor(cw);}

  output.Ellipse(X-3,y-3,x+3,y+3);
 End;

Procedure TmechGraph.Opora(x,y: integer; name: string);
 {����������� ������������ ���� ������ (����������� �����������)}
Begin
  {SetColor(cw);
  SetWriteMode(0); }

  output.MoveTo(x,y);
  output.Lineto(x - 7,y + 7);
  output.lineto(x + 7,y + 7);
  output.lineto(x,y);
  {SetLineStyle(0,0,3); }
  output.Moveto(x - 10,y + 10);
  output.lineto(x + 10,y + 10);
  {SetLineStyle(0,0,1);
  SetFillStyle(1,cw);    }
  Circ(x,y);
  OutTextXY(x+ 4,y- 8,Name);
  
End;

Procedure TmechGraph.Opora90(x,y: integer; Name: String);
  {����������� ������������ ���� ������, ���������� �� 90 �������� �����}
 Begin
  {SetColor(cw);
  SetWriteMode(0); }

  output.MoveTo(x,y);
  output.Lineto(x + 7,y + 7);
  output.lineto(x + 7,y - 7);
  output.lineto(x,y);
  {SetLineStyle(0,0,3); }
  output.Moveto(x + 10,y + 10);
  output.lineto(x + 10,y - 10);
  {SetLineStyle(0,0,1);
  SetFillStyle(1,cw);   }
  Circ(x,y);
  OutTextXY(x+ 12,y- 18,Name);

End;

Procedure TmechGraph.OporaPost(x,y: integer; FI: Real; Name: String);
      { ����������� �������������� ���� ������, }
      { ���� ���� ��� ���� �� ������������      }
 Const Ly=11.2;
       Lx=20.2;
 Var sf,cf:real;
     xc,yc,xk,yk:integer;
  Begin
   {PPair(x,y,Fi);}

   sf:=Sin(Fi);
   cf:=Cos(Fi);
   xc:=x+Round(Ly*sf);
   yc:=y+Round(Ly*Cf);
   xk:=Round(Lx*Cf);
   yk:=Round(Lx*Sf);
   {SetColor(cw);
   SetWriteMode(0);
   SetLineStyle(0,0,3);}

   Line(xc-xk,yc+yk,xc+xk,yc-yk);

   {SetLineStyle(0,0,1);  }
   OutTextXY(x+ 15,y- 10,Name);
  End;

Procedure TmechGraph.NamZ(xl,yl,xr,yr: integer; Name: String);
 Var xs,ys:integer;
 Begin
  xs:=(xl+xr) div 2+5;
  ys:=(yl+yr) div 2+5;
  OutTextXY(xs,ys,Name);
 End;
Procedure TmechGraph.InitLink(xst,yst,xa,ya: integer; Name: String);
          {����������� ���������� ����� - ��� ���� ������������}
 {xst,yst - ���������� ������, xa,ya -���������� ������� ������������ ���� A }
 Begin
  {SetColor(cw);
  SetWriteMode(0);  }
  Line(xst,yst,xa,ya);
  Circ(xst,yst);
  Circ(xa,ya);
  OutTextXY(xa+ 2,ya+ 2,Name);
 End;

Procedure TmechGraph.Diada1g(xl,yl,xs,ys,xr,yr: integer; Name: String);
     {         ����������� ������ ������� ����          }
     {xl,yl � xr,yr - ���������� ������� �������� ������}
     {    xs,ys  - ���������� ����������� �������       }
 Begin
  {SetColor(cw);
  SetWriteMode(0); }
  output.Moveto(xl,yl);
  output.Lineto(xs,ys);
  output.Lineto(xr,yr);
  Circ(xl,yl);
  Circ(xs,ys);
  Circ(xr,yr);
  OutTextXY(xs+ 4,ys+ 4,Name);
 End;

 Procedure TmechGraph.Draw(ppg:TPoint; pFI:Real; var PP: Point5);
{ ������ ��������� ������� ����� �������������� ���� }
 Const
     Tx=11.6; Ty=6.6;
  Var
    j : integer;
    Txc,Tyc,Txs,Tys,c,s : real;
    Px,Py: Integer;
 Begin
     S:=SIN(pFI); C:=COS(pFI);
     Txc:=Tx*c; Tyc:=Ty*c; Txs:=Tx*s; Tys:=Ty*s;
     Px := Round((Txc - Tys));  Py := Round(Txs + Tyc);
     PP[1].x:= ppg.x + Px; PP[1].y:= ppg.y - Py;
     PP[3].x:= ppg.x - Px; PP[3].y:= ppg.y + Py;
     Px := Round((- Txc - Tys));  Py := Round(- Txs + Tyc);
     PP[2].x:= ppg.x + Px; PP[2].y:= ppg.y - Py;
     PP[4].x:= ppg.x - Px; PP[4].y:= ppg.y + Py;
     PP[5].x:=PP[1].x; PP[5].y:=PP[1].y;
     For j:=1 to 4 do
      begin
       PT[j].X:=(PP[j].X+PP[j+1].X) DIV 2;
       PT[j].Y:=(PP[j].Y+PP[j+1].Y) DIV 2;
      end;
 End; { Procedure Draw }

Procedure TmechGraph.PPairE(pgx,pgy:integer; FI:Real);
{ ����������� �������������� ���� �� ��������� ���������� ������� }
 Var P:Point5;
     PG:TPoint;
 Begin
  {SetColor(cw);
  SetFillStyle(1,GetBkColor);
  SetWriteMode(0);        }
  PG.x:=pgx; PG.y:=pgy;
   Draw(pg,Fi,P);
  output.Polygon(P);
 End; { Procedure PPairE }

Procedure TmechGraph.PPair(pgx,pgy:integer; FI:Real);
{ ����������� �������������� ���� ��� �������� ������}
 Var P:Point5;
     PG:TPoint;
 Begin
  {SetColor(cw);
   SetWriteMode(0);     }
   PG.x:=pgx; PG.y:=pgy;
   Draw(pg,Fi,P);
     output.Polyline(P);
 End; { Procedure PPair }

Procedure TmechGraph.Diada2g(xl,yl,xs,ys,xp,yp: integer; fip: real; Name: String);
 {          ����������� ������ ������� ����                   }
 {xl,yl - ���������� ������� ������������ ���� ������         }
 {xs,ys - ���������� ���������� ������������ ���� ������      }
 {xp,yp - ���������� ������ ������� �������������� ���� ������}
 {fip - ���� ������� �������������� ���� (� ��������)         }
 Begin
   If (xl<>xs) or (yl<>ys) then
    Line(xs,ys,xp,yp);
  PPairE(xp,yp,fip);
  Line(xl,yl,xs,ys);
  Circ(xl,yl);
  Circ(xs,ys);
  OutTextXY(xs+ 4,ys+ 4,Name);
 End;

Procedure TmechGraph.Diada3g(xa,ya,xb,yb,xc,yc,xd,yd: integer; fi: real; Name: String);
 {         ����������� ������ �������� ���� (�������� ��������)           }
 {xa,ya - ���������� ������� ������������ ���� �� �����                   }
 {xb,yb - ���������� ������ ���������� �������������� ���� (�����)        }
 {���� fi - ���� ������� �������������� ���� (��������� � �������� ������)}
 {xc,yc - ���������� ������� �������������� ���� �� ������                }
 {xd,yd - ���������� ��������� ��������� ����� �� ������                  }
Begin
  Line(xc,yc,xd,yd);
   If (xa<>xb) or (ya<>yb) Then
    Line(xa,ya,xb,yb);
  PPairE(xb,yb,fi);
  Circ(xa,ya);
  OutTextXY(xb+ 8,yb+ 4,Name);
End;

Procedure TmechGraph.Diada4g(xa,ya,xb,yb,xc,yc: integer; fi2,fi3: real; Name: String);
  {����������� ������ 4-�� ����}
  {xa,ya � xc,yc - ���������� ������� ������� �������������� ���}
  {fi2 � fi3 - ��������������� ���� �� ������� (� ��������)}
  {xb,yb - ���������� ���������� ������������ ����}

 Begin
   If (xa<>xb) or (ya<>yb) Then
    Line(xa,ya,xb,yb);
   If (xc<>xb) or (yc<>yb) Then
    Line(xc,yc,xb,yb);
  PPairE(xa,ya,fi2);
  PPairE(xc,yc,fi3);
  Circ(xb,yb);
  OutTextXY(xb+ 4,yb+ 4,Name);
 End;

Procedure TmechGraph.Diada5g(xr,yr,xop2,yop2,xop3,yop3,x4,y4,x5,y5: integer;
          fi2,fi3: real; Name:String);
{����������� ������ 5-�� ����}
{xr,yr - ���������� ������������ ����. }
{xop2,yop2 - ����� ���������� �������������� ����.}
{xop3,yop3 - ����� ������� �������������� ����.}
{x4,y4 - ���������� �������� ����� �� ������������ ������� ������. ����}
{x5,y5 - ��� ���� ����� �� ������������ ������� �������������� ����}
{fi2 � fi3 - ���� ������� ������������ ���������� � ������� ������. ���}
Begin
{ SetColor(cw);
  SetWriteMode(0);}
  output.MoveTo(x4,y4);
  output.LineTo(x5,y5);
  output.MoveTo(xr,yr);
  output.LineTo(xop2,yop2);
  output.LineTo(xop3,yop3);
  PPairE(xop3,yop3,fi3);
  PPairE(xop2,yop2,fi2);
  Circ(xr,yr);
  OutTextXY(xop2+15,yop2-15,Name);

End;

Procedure TmechGraph.Row;
Var
  xx1,yy1,xr,yr : integer;
Begin { Row }
  xr := x; yr := y;
  xx1 := x + round((- 5)*cos(alfa) - 2*sin(alfa));
  yy1 := y - round((- 5)*sin(alfa) + 2*cos(alfa));
  output.moveto(xr,yr); output.lineto(xx1,yy1);
  xx1 := x + round((- 5)*cos(alfa) + 2*sin(alfa));
  yy1 := y - round((- 5)*sin(alfa) - 2*cos(alfa));
  output.moveto(xr,yr); output.lineto(xx1,yy1);
End;  { Row }
                          {��������������� ��������� ��������� �������}
  Procedure TmechGraph.ArrD(px,py:integer);
   {C������ ����. ������ � px,py, ����� - 20 �������� }
  Begin
   Line(px,py,px,py-20);
   Line(px-1,py-2,px-1,py-5);
   Line(px+1,py-2,px+1,py-5);
   Line(px-2,py-4,px-2,py-5);
   Line(px+2,py-4,px+2,py-5);
  End;

  Procedure TmechGraph.ArrL(px,py:integer);
   {C������ �����. ������ � px,py, ����� - 20 �������� }
  Begin
   Line(px,py,px+20,py);
   Line(px+2,py-1,px+5,py-1);
   Line(px+2,py+1,px+5,py+1);
   Line(px+4,py-2,px+5,py-2);
   Line(px+4,py+2,px+5,py+2);
  End;
  Procedure TmechGraph.Vect(px,py,vx,vy:integer);
  var
  len:double;
  lx,ly,vx1,vy1:integer;
  begin
    len:=sqrt(vx*vx+vy*vy);
    if len=0 then len:=1;
    lx:=-round(vy/len);
    ly:=round(vx/len);
    vx1:=round(4*vx/len);
    vy1:=round(4*vy/len);
    Line(px,py,px+vx,py+vy);
    Line(px+vx,py+vy,px+vx-vx1+lx*1,py+vy-vy1+ly*1);
    Line(px+vx,py+vy,px+vx-vx1-lx*1,py+vy-vy1-ly*1);
  end;
BEGIN
 cw:=15;
End.
