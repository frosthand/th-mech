Unit fDi5;  {������ �������������� ���������� ����� ���� ���� �����}
{$N+}
Interface
Uses Math;
Const Err = 1E-12;
      PI05=PI/2;
      Pi2 =Pi*2;
type
    

    Point=object
         x,y,vx,vy,ax,ay:double;
         opora:boolean;
    end;
    Mechanism=object
                    Points: array[1..10] of point;
    end;
Var mns:boolean;
 Procedure NachZweno(ox,oy, l1, fi,omega,epsi: double;
           Var ax,ay,vax,vay,aax,aay: double);
 Procedure Diada1v(sb:byte; ax,ay,vax,vay,aax,aay,
                   cx,cy,vcx,vcy,acx,acy, l2,l3: double;
           Var fi2,omega2,epsi2, fi3,omega3,epsi3, bx,by,vbx,vby,abx,aby,
                   aNBA,fiNBA,aNBC,fiNBC: double);
 Procedure Diada1vP(sb:byte; ax,ay,cx,cy, l2,l3: double;
           Var fi2,fi3, bx,by: double);
 Procedure Diada2v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy,
                   fi4,omega4,epsi4, l2,l3: double;
           Var fi2,omega2,epsi2, l4,v4,a4, brx,bry,vbx,vby,abx,aby, bpx,bpy,
                aNBA,fiNBA,aKB4B,fiKB4B: double);
 Procedure Diada2vP(ax,ay, cx,cy, fi4, l2,l3: double;
           Var fi2, l4, brx,bry, bpx,bpy: double);
 Procedure Diada3v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy, l2: double;
           Var fi3,omega3,epsi3, l3,v3,a3, bpx,bpy,
               aNA3C,fiNA3C,aKA3A,fiKA3A: double);
 Procedure Diada3vP(ax,ay, cx,cy, l2: double; Var fi3,l3, bpx,bpy: double);
 Procedure Diada4v(ax,ay,vax,vay,aax,aay, fi1,omega1,epsi1,
                   cx,cy,vcx,vcy,acx,acy, fi4,omega4,epsi4, l2,l3: double;
           Var l1,v1,a1, l4,v4,a4, bx,by,vbx,vby,abx,aby, apx,apy, cpx,cpy,
               akBB1,fikBB1, akBB4,fikBB4: double);
 Procedure Diada4vP(ax,ay, fi1, cx,cy, fi4, l2,l3: double;
           Var l1,l4, bx,by, apx,apy, cpx,cpy: double);
 Procedure Diada5v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy,
                   l2,fi34, fi4,omega4,epsi4: double;
           Var fil2,fil3, lCp,vCp,aCp, lBp,vBp,aBp, xCp,yCp, xBp,yBp,
               akA3A4,fikA3A4,akA3A2,fikA3A2: double);
 Procedure Diada5vP(ax,ay, cx,cy, l2,fi34, fi4: double;
           Var fil2,fil3, lCp,lBp, xCp,yCp, xBp,yBp: double);

Implementation

 Const DvaPi=Pi+Pi;
       PolPi=Pi/2;
 Type real = double;

FUNCTION UG05(FI,US:Double):Double;
         {������� FI �� +PI/2 ��� �� -PI/2 � ����������� �� ����� US}
 BEGIN
  IF US>=0 THEN
   UG05:=FI+PI05
  ELSE
   UG05:=FI-PI05
 END;

FUNCTION UGPI(FI,US:Double):Double;
         {������� FI �� +PI ��� �� -PI � ����������� �� ����� US}
 BEGIN
  IF US>=0 THEN
   UGPI:=FI
  ELSE
   UGPI:=FI-PI
 END;

FUNCTION ARTFI(DX,DY:Double):Double;
         {������� ����������� �� ���� ������� ���������}
 VAR FI:Double;
 BEGIN
  IF DX=0 THEN
   begin
     IF DY>=0 THEN
      FI:=PI05
     ELSE
      FI:=-PI05;
    artfi:=fi;
   end
  ELSE
   BEGIN
    FI:=arctan(DY/DX);
     IF DX>=0 THEN
      ARTFI:=FI
     ELSE
      ARTFI:=FI+PI
   END
 END;

Procedure XYRFi(x,y: double; var r,fi:double);
       {������� �� ���������� ��������� � ��������}
 Begin
  r:=sqrt(sqr(x)+sqr(y));
   if r=0 then
    fi:=0
   else
    fi:=ArtFi(x,y);
 End;
function Dist(x,y:double):double;
begin
  Dist:=sqrt(sqr(x)+sqr(y));
end;
PROCEDURE PROXY(P1,AL1,P2,AL2,P3,AL3:Double;VAR P0,AL0:Double);
         {�������� ���� ��������}
 VAR PX,PY:Double;
 BEGIN
   PX:=P1*cos(AL1)+P2*cos(AL2);
   PY:=P1*sin(AL1)+P2*sin(AL2);
    IF P3<>0 THEN
     BEGIN
      PX:=PX+P3*cos(AL3); PY:=PY+P3*sin(AL3)
     END;
    If (PX=0) and (PY=0) then
     P0:=0
    else
     P0:=Dist(PX,PY);
    If P0=0 Then
     AL0:=0
    else
     AL0:=ARTFI(PX,PY)
 END;

FUNCTION FIOGR(FI: double): double;
       { ������������ ���� Fi - ���������� � ��������� <-Pi +Pi> }
 VAR PP: double;
 BEGIN
  PP:=FI;
   IF ABS(PP)>PI THEN
    BEGIN
     WHILE PP<0 DO
      PP:=PP+PI2;
     WHILE PP>PI DO
      PP:=PP-PI2
    END;
  FIOGR:=PP
 END;

PROCEDURE ALGUR(PAL1,PAL2,PR1,PRAL1,PR2,PRAL2,PR3,PRAL3,PR4,PRAL4:Double;
                VAR PL1,PL2:Double);
 {����������� ���� �������� PL1 � PL2 ���������� ����������� PAL1 � PAL2,}
 {���������� ����� ������� �������� �������� PR1, PR2, PR3 � PR4.}
 VAR PSI:REAL;
 BEGIN
  PSI:=sin(PAL1-PAL2);
   IF ABS(PSI)<ERR THEN
    BEGIN
     {C(CA);WRITELN('*** MEPTBOE �O�O�EH�E');C(CM);}
     mns:=true
    END
   ELSE
    BEGIN
     PL2:=PR1*sin(PRAL1-PAL1);
     PL2:=PL2+PR2*sin(PRAL2-PAL1);
     PL1:=PR1*sin(PRAL1-PAL2);
     PL1:=PL1+PR2*sin(PRAL2-PAL2);
      IF PR3<>0 THEN
       BEGIN
        PL2:=PL2+PR3*sin(PRAL3-PAL1);
        PL1:=PL1+PR3*sin(PRAL3-PAL2)
       END;
      IF PR4<>0 THEN
       BEGIN
        PL2:=PL2+PR4*sin(PRAL4-PAL1);
        PL1:=PL1+PR4*sin(PRAL4-PAL2)
       END;
     PL1:=PL1/PSI; PL2:=PL2/PSI
    END
  END;

PROCEDURE VWTXY(PXA,PYA,PVA,PALVA,PWA,PBEWA, POM,PEP, PXB,PYB:Double;
                VAR PVB,PALVB,PWB,PBEWB: Double);
  { ����������� �������� � ��������� ����� B �����                      }
  { ��� ��������� �������� � ��������� ��������� ����� A ����� �� �����,}
  { � ����� ��������� ������� �������� � ������� ��������� �����.       }
  { ����� ������ ������������, �������� � ��������� - ��������� (r,fi)  }
  VAR PDX,PDY,PL,PFI : Double;
      PVBA,PALBA,PWNBA,PBEAN,PWTBA,PBEAT : Double;
  BEGIN
   PDX:=PXB-PXA; PDY:=PYB-PYA;
                                   { ��������� ������� AB }
   {PL:=Module(PDX,PDY); PFI:=ARTFI(PDX,PDY);}
                       {������� � �������� ����������}
   XYRFi(PDX,PDY,PL,PFI);
   PVBA:=ABS(POM*PL);
   PALBA:=UG05(PFI,POM);        { �������� B }
   PROXY(PVA,PALVA,PVBA,PALBA,0,0,PVB,PALVB);

   PWNBA:=ABS(PVBA*POM); PBEAN:=PFI+PI;
   PWTBA:=ABS(PEP*PL);
   PBEAT:=UG05(PFI,PEP);        { ��������� B }
   PROXY(PWA,PBEWA,PWNBA,PBEAN,PWTBA,PBEAT,PWB,PBEWB)
  END;

  Procedure XY(X1,Y1,PL,FR: double; var X2,Y2: double);
           { ���������� ��������� X2,Y2 ����� ������� ������ PL � ��������}
           { � ��� ������� ��� ����� FR � � ������� � ����� X1,Y1}
  BEGIN
   X2:=X1+PL*cos(FR);
   Y2:=Y1+PL*sin(FR)
  END;

 PROCEDURE CROS(X1,Y1,FI1,X2,Y2,FI2:double; VAR X0,Y0:double);
         { ���������� ��������� X0,Y0 ����� ����������� ���� ������,}
         { �������� ������������ ����� � ����� �������}
  VAR SI1,SI2,CO1,CO2,SI:double;
  BEGIN
   SI:= sin(FI1-FI2);
   SI1:=sin(FI1); SI2:=sin(FI2);
   CO1:=cos(FI1); CO2:=cos(FI2);
   X0:= (Y2-Y1)*CO1*CO2-X2*CO1*SI2+X1*SI1*CO2;
   Y0:= -(X2-X1)*SI1*SI2+Y2*CO2*SI1-Y1*SI2*CO1;
   X0:= X0/SI; Y0:=Y0/SI
  END;

 Procedure CROS90(X1,Y1,FR,X2,Y2: real; var X0,Y0,S,H: real);
           { ��������� �������������� �� ����� X2,Y2 �� ������,     }
           { �������� ������ X1,Y1 � ����� FR.                      }
           { ������������ ���������� X0,Y0 ��������� ��������������,}
           { ��� ����� H � ���������� S �� X1,Y1 �� X0,Y0           }
  var DX,DY,SI,CO: real;
  BEGIN
   DX:=X2-X1;
   DY:=Y2-Y1;
   SI:=sin(FR);
   CO:=cos(FR);
   H:=DY*CO-DX*SI;
   S:=DY*SI+DX*CO;
   XY(X1,Y1,S,FR,X0,Y0)
  END;

FUNCTION MEN(FI1,FI2:Double):BOOLEAN;
                    {��������� ���� �����}
  VAR PP:Double;
  BEGIN
   PP:=FIOGR(FI1-FI2);    {������� ������������}
    IF PP>=0 THEN
     MEN:=FALSE    {���� Fi1 ������ ���� Fi2}
    ELSE
     MEN:=TRUE     {���� Fi1 ������ ���� Fi2}
  END;

Procedure NachZweno(ox,oy, l1, fi,omega,epsi: double;
                 var ax,ay,vax,vay,aax,aay: double);
    {���������� �������������� ���������� ����� A ���������� ����� l1}
    {O - ����� ������}
 {                 A o   }
 {          l1     /     }
 {             \ /       }
 {             /         }
 {        O  o           }
 {                       }

 Var lco,lsi: double;
 Begin
  lco:= l1* cos(fi);
  lsi:= l1* sin(fi);
  ax:= ox+ lco; ay:= oy+ lsi;
  vax:= - omega* lsi; vay:= omega* lco;
  aax:= - Sqr(omega)* lco- epsi* lsi;
  aay:= - Sqr(omega)* lsi+ epsi* lco;
 End; {Nach}

 Procedure Diada1v(sb:byte; ax,ay,vax,vay,aax,aay,
          cx,cy,vcx,vcy,acx,acy, l2,l3: double;
      Var fi2,omega2,epsi2, fi3,omega3,epsi3, bx,by,vbx,vby,abx,aby,
          aNBA,fiNBA,aNBC,fiNBC: double);
       { ����� 1-�� ���� (���  ���� A, B � C - ������������) }
       { sb - ������� ������ ( 1 (��) ��� 2(������ ������� �������))}
       { ������� fi2=999 �������� �������������� ������     }

  {          B o                sb - ������� ������
 {   l2     /   \    l3     <-- ��� ���� ����� sb=1
 {      \ /       \/
 {      /           o  C
 { A  o
 {                              }

 Var rab,si,co:double;
  AC,fiAC,FI,LBCA,XB1,YB1: double;
  VA,ALVA,VC,ALVC,VB,ALVB,VBA,ALVBA,VBC,ALVBC:DOUBLE;
  WA,BETWA,WC,BETWC,WB,BETWB,WNBA,BETWNBA,WNBC,BETWNBC,
  WTBC,WTBA,BETBA,BETBC: double;

 Begin   { Diada1v }
  AC:=Dist(cx-ax,cy-ay);
    IF AC<ERR THEN
     BEGIN
      fi2:=999;
      EXIT;
     END;
    IF (AC >= L2+L3) or (ABS(L2-L3) >= AC) then
     BEGIN
      fi2:=999;
      EXIT;
     END;
   fiAC:=ARTFI(ax-cx,ay-cy);
    FI:=FIAC-PI05;
   LBCA:=(SQR(L2)+SQR(AC)-SQR(L3))/AC/2;
   YB1:=AC-LBCA;
   XB1:=L2*SQRT(ABS(1-SQR(LBCA/L2)));
    IF (sb<>1) THEN
     XB1:=-XB1;
   SI:=SIN(FI); CO:=COS(FI);
   bx:=XB1*CO-YB1*SI+cx;
   by:=XB1*SI+YB1*CO+cy;
   fi2:=ARTFI(bx-ax,by-ay);
   fi3:=ARTFI(bx-cx,by-cy);
          {�������� � ���������}
           {��������}
            {������� � �������� �������}
   XYRFi(vax,vay,VA,ALVA);
   XYRFi(vcx,vcy,VC,ALVC);
   XYRFi(aax,aay,WA,BETWA);
   XYRFi(acx,acy,WC,BETWC);
                      {������������� ��������}
   ALGUR(fi2+PI05,fi3+PI05,VC,ALVC,-VA,ALVA,0,0,0,0,VBA,VBC);
                      {�� ���������� �����������}
   ALVBA:=UG05(fi2,VBA);
   ALVBC:=UG05(fi3,VBC);
   {PVBA:=ABS(PVBA);
   PVBC:=ABS(PVBC);}  {������ �������� ���� B}
   PROXY(VA,ALVA,abs(VBA),ALVBA,0,0,VB,ALVB);
    vbx:=VB*cos(ALVB); vby:= VB*sin(ALVB);
   Omega2:=-Abs(VBA)/L2;
    IF MEN(fi2,ALVBA) THEN
     Omega2:=-Omega2;
   Omega3:=-Abs(VBC)/L3;
    IF MEN(fi3,ALVBC) THEN
     Omega3:=-Omega3;
                {���������}
                   {���������� ���������}
   WNBA:=SQR(Omega2)*L2;
   BETWNBA:=fI2+Pi;
   WNBC:=SQR(Omega3)*L3;
   BETWNBC:=FI3+PI;
   aNBA:=WNBA;
   fiNBA:=BETWNBA;
   aNBC:=WNBC;
   fiNBC:=BETWNBC;
                 {����������� ����������� ���������}
   ALGUR(fi2+PI05,fi3+PI05,WC,BETWC,-WA,BETWA,WNBC,BETWNBC,
        -WNBA,BETWNBA,WTBA,WTBC);
                 {�� ���������� �����������}
   BETBA:=UGPI(fi2+PI05,WTBA);
   BETBC:=UGPI(fi3+PI05,WTBC);
   {PWTBA:=ABS(PWTBA);
   PWTBC:=ABS(PWTBC);} {������ ��������� B}
   PROXY(WA,BETWA,WNBA,fi2+PI,Abs(WTBA),BETBA,WB,BETWB);
   abx:= WB*cos(BETWB); aby:= WB*sin(BETWB);
                        {������� ���������}
       Epsi2:=-Abs(WTBA)/L2;
    IF MEN(fi2,BETBA) THEN
     Epsi2:=-Epsi2;
   Epsi3:=-Abs(WTBC)/L3;
    IF MEN(fi3,BETBC) THEN
     Epsi3:=-Epsi3;

 End; { Diada1v }

 Procedure Diada1vP(sb:byte; ax,ay,cx,cy, l2,l3: double;
           Var fi2,fi3, bx,by: double);
     {����������� ������� ��������� Diada1v -������ ���������}
       { ����� 1-�� ���� (���  ���� A, B � C - ������������) }
       { sb - ������� ������ ( 1 (��) ��� 2(������ ������� �������))}
       { ������� fi2=999 �������� �������������� ������     }

  {          B o                sb - ������� ������
 {   l2     /   \    l3     <-- ��� ���� ����� sb=1 - ABC �� ������� �������
 {      \ /       \/            
 {      /           o  C
 { A  o
 {                              }

 Var si,co:double;
  AC,fiAC,FI,LBCA,XB1,YB1: double;
 Begin   { Diada1v }
  AC:=Dist(cx-ax,cy-ay);
    IF AC<ERR THEN
     BEGIN
      fi2:=999;
      EXIT;
     END;
    IF (AC >= L2+L3) or (ABS(L2-L3) >= AC) then
     BEGIN
      fi2:=999;
      EXIT;
     END;
   fiAC:=ARTFI(ax-cx,ay-cy);
    FI:=FIAC-PI05;
   LBCA:=(SQR(L2)+SQR(AC)-SQR(L3))/AC/2;
   YB1:=AC-LBCA;
   XB1:=L2*SQRT(ABS(1-SQR(LBCA/L2)));
    IF NOT (sb=1) THEN
     XB1:=-XB1;
   SI:=SIN(FI); CO:=COS(FI);
   bx:=XB1*CO-YB1*SI+cx;
   bY:=XB1*SI+YB1*CO+cy;
   fi2:=ARTFI(bx-ax,by-ay);
   fi3:=ARTFI(bx-cx,by-cy);
   END;{DIada1vP}

  Procedure Diada2v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy,
                                          fi4,omega4,epsi4, l2,l3: double;
        Var fi2,omega2,epsi2, l4,v4,a4, brx,bry,vbx,vby,abx,aby, bpx,bpy,
                                         aNBA,fiNBA,aKB4B,fiKB4B: double);
        { ����� 2-�� ���� - ���� �������������� ���� (�������) - ���� Bp  }
        { ������ ������������ ����� fi4 ( fi4 - ������������ ��� �� �����,}
        { � fi4 = fi4 + Pi ��� ������ ������, ����� ���� Bp ����� ����� A)}
        { ������� fi2=999 �������� �������������� ������                  }

   {             B o            }
   {        l2   / | - l3    l4 }
   {   C      \/  _|_      /    } {brx,bry - ���������� ������������ ����  }
   { -*------/---|_ _|------> + } {bpx,bpy - ���������� ������ ������. ����}
   {       /         Bp         } { C - �������� ����� �� ������������ 4.  }
   {   A o                      } { Fi4 - ���� ������� ������������ 4.     }

   { �������������� ���� Bp ����������� �� ������������ 4 � ������������� }
   { ����������� �� �������� ���� A �� ������������ 4 ( �� ����� ������)  }
   { ���� ���������� ���������� ���� Bp ����� �������� ���� A �� ������. 4,}
   { �� ���� fi4 ����� �������� �� ���� fi4+Pi.                           }
   { ��� l3>0 ���� B �� ��������� � ������������ 4 ����������� ���, ���  }
   { ���� ������� Bp-B ����� Fi4+Pi/2, �.�. ����� �� �������. ����������� }
   { ���� ���������� �����, �� ����� ������� ���� l3                      }
   { WNBA � WKB1B - �������������� ���������� ��������� �.B ������������ A �}
   { ����������� ��������� �.B4, ������������� ����� l4 ������������ �.B}
   { FINBA � FIKB4B - ���� ������� ���� ��������}
  Var
    rab,fi3,AC,x0,y0: double;
    VC,ALVC,WC,BETWC,VA,ALVA,WA,BETWA,VB4,ALVB4,WB4,BETWB4: double;
    VBA,ALBA,ALBC,VB,ALVB: double;
    PWNBA,PFINBA,PWKB4B,PFIKB4B,WTBA,FITBA,WB,BETWB,ALWA4: double;
  Begin { Diada2v}

    fi3:=fi4+PI05;
   CROS90(cx,cy,fi4,ax,ay,X0,Y0,rab,rab);
    IF L3<>0 THEN
     XY(X0,Y0,L3,fi3,X0,Y0);
   AC:=Dist(ax-X0,ay-Y0);
    IF AC>L2 THEN
     BEGIN
      fi2:=999;
      EXIT;
     END;
   L4:=SQRT(SQR(L2)-SQR(AC));
   XY(X0,Y0,L4,fi4,brx,bry);
   fi2:=ARTFI(brx-ax,bry-ay);
    IF L3=0 THEN
     BEGIN
      bpx:=brx;
      bpy:=bry;
     END
    ELSE
     CROS90(cx,cy,fi4,brx,bry,bpx,bpy,rab,rab);
     {�������� � ���������}
      {��������}
       {������� � �������� �����������}
   XYRFi(vcx,vcy,VC,ALVC);
   XYRFi(acx,acy,WC,BETWC);
   XYRFi(vax,vay,VA,ALVA);
   XYRFi(aax,aay,WA,BETWA);
            {����������� ���������� �������� �.B, ������������� ����� 4}
   VWTXY(cx,cy,VC,ALVC,WC,BETWC,omega4,epsi4,brx,bry,VB4,ALVB4,WB4,BETWB4);
             {������ ������������� ��������}
   ALGUR(fi2+PI05,fi4,VB4,ALVB4,-VA,ALVA,0,0,0,0,VBA,v4);
             {�� �������� �����������}
   ALBA:=UG05(fi2,VBA);
   ALBC:=UGPI(fi4,v4);
   {PVBA:=ABS(PVBA);
   PVBC:=ABS(PVBC); }
   Omega2:=-Abs(VBA)/L2;
    IF MEN(fi2,ALBA) THEN
     Omega2:=-Omega2;      {������ �������� B}
   PROXY(VA,ALVA,ABs(VBA),ALBA,0,0,VB,ALVB);
   vbx:=VB*cos(ALVB); vby:=VB*sin(ALVB);
         {���������}
                 {���������� ������������ �����������}
   PWNBA:=SQR(Omega2)*L2;
   PFiNBA:=fi2+Pi;
                 {����������� ���������}
   PWKB4B:=2*ABS(Omega4*v4);
   PFIKB4B:=UG05(ALBC,Omega4);
   aNBA:=PWNBA;
   fiNBA:=PFiNBA;
   aKB4B:=PWKB4B;
   fiKB4B:=PFIKB4B;
                 {����������� ������������� ���������}
   ALGUR(ALBA,fi4,WB4,BETWB4,-WA,BETWA,PWKB4B,PFIKB4B,
        -ABS(PWNBA),PFINBA,WTBA,a4);
                  {�� ���������� �����������}
   FITBA:=UGPI(ALBA,WTBA);
   ALWA4:=UGPI(fi4,a4);
  { PWTBC:=ABS(PWTBC);
   {PWTBA:=ABS(PWTBA);}
                       {������� ��������� L2}
   Epsi2:=-Abs(WTBA)/L2;
    IF MEN(fi2,FITBA) THEN
     Epsi2:=-Epsi2;
                      {������ ��������� ���� B}
   PROXY(WB4,BETWB4,Abs(PWKB4B),PFIKB4B,A4,FI4,WB,BETWB);
   abx:=WB*cos(BETWB); aby:=WB*sin(BETWB);
  End; { Diada2v }

  Procedure Diada2vP(ax,ay, cx,cy, fi4, l2,l3: double;
        Var fi2, l4, brx,bry, bpx,bpy: double);
      {����������� ������� �������� Diada2v - ������ ���������}
        { ����� 2-�� ���� - ���� �������������� ���� (�������) - ���� Bp  }
        { ������ ������������ ����� fi4 ( fi4 - ������������ ��� �� �����,}
        { � fi4 = fi4 + Pi ��� ������ ������, ����� ���� Bp ����� ����� A)}
        { ������� fi2=999 �������� �������������� ������                  }

   {             B o            }
   {        l2   / | - l3    l4 }
   {   C      \/  _|_      /    } {brx,bry - ���������� ������������ ����  }
   { -*------/---|_ _|------> + } {bpx,bpy - ���������� ������ ������. ����}
   {       /         Bp         } { C - �������� ����� �� ������������ 4.  }
   {   A o                      } { Fi4 - ���� ������� ������������ 4.     }

   { �������������� ���� Bp ����������� �� ������������ 4 � ������������� }
   { ����������� �� �������� ���� A �� ������������ 4 ( �� ����� ������)  }
   { ���� ���������� ���������� ���� Bp ����� �������� ���� A �� ������. 4,}
   { �� ���� fi4 ����� �������� �� ���� fi4+Pi.                           }
   { ��� l3>0 ���� B �� ��������� � ������������ 4 ����������� ���, ���  }
   { ���� ������� Bp-B ����� Fi4+Pi/2, �.�. ����� �� �������. ����������� }
   { ���� ���������� �����, �� ����� ������� ���� l3                      }

  Var
    rab,fi3,AC,x0,y0: double;

  Begin { Diada2v}

    fi3:=fi4+PI05;
   CROS90(cx,cy,fi4,ax,ay,X0,Y0,rab,rab);
    IF L3<>0 THEN
     XY(X0,Y0,L3,fi3,X0,Y0);
   AC:=Dist(ax-X0,ay-Y0);
    IF AC>L2 THEN
     BEGIN
      fi2:=999;
      EXIT;
     END;
   L4:=SQRT(SQR(L2)-SQR(AC));
   XY(X0,Y0,L4,fi4,brx,bry);
   fi2:=ARTFI(brx-ax,bry-ay);
    IF L3=0 THEN
     BEGIN
      bpx:=brx;
      bpy:=bry;
     END
    ELSE
      CROS90(cx,cy,fi4,brx,bry,bpx,bpy,rab,rab);
  End; { Diada2vP }

  Procedure Diada3v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy, l2: double;
                  Var fi3,omega3,epsi3, l3,v3,a3, bpx,bpy,
                                          aNA3C,fiNA3C,aKA3A,fiKA3A: double);
          {������ 3-�� ���� - ���� �������������� (����������) ���� B }
          {����� bp - ��������� �������������� �� A �� ����������� l3 }
          {������� fi3=999 �������� �������������� ������             }

   {    A  o    l2     + }
   {         \ /     /   }
   {           \ /\/     }
   {           /   /     }
   {           \ /  Bp   }
   {         /           }
   {       / l3          }
   {  C  o               }

   { ��� l2<>0 ���� A ����������� ����� �� Fi3 ��� l2>0 � ������ ��� l2<0 }
   { aNA3C � aKA3A - �������������� ���������� ��������� �.A3 }
   { ������������ �.C � ����������� ��������� �.A3 ������������ ���� A. }
   { fiNA3C � fiKA3A - ���� ������� ���� �������}
  Var rab,AC,fiAC,psi: double;
      VA,ALVA,VC,ALVC,VA3C,ALVA3C,VA3A,ALVA3A,VB,ALVB: double;
      WA,BETWA,WC,BETWC,PWNA3C,PFINA3C,PWKA3A,PFIKA3A,WTA3C,FITA3C:double;
      WA3,BETWA3:double;
  Begin   { Diada3v }
   AC:=Dist((ax-cx),(ay-cy));
   IF (AC<ABS(l2)) OR (AC<Err) THEN
     BEGIN
      fi3:=999;
      exit;
     END;
   fiAC:=ARTFI((ax-cx),(ay-cy));
    IF l2=0 THEN
     BEGIN
      l3:=AC;
      fi3:=fiAC;
      bpx:=ax;
      bpy:=ay;
     END
    ELSE   {l2<>0}
     BEGIN
      PSI:=sin(l2/AC);
      fi3:=fiAC-PSI;
      CROS90(cx,cy,fi3,ax,ay,bpx,bpy,l3,rab);
     END;
        {�������� � ���������}
          {��������}
            {������� � �������� ���������}
   XYRFi(vax,vay,VA,ALVA);
   XYRFi(vcx,vcy,VC,ALVC);
             {����������� ������������� ���������}
   ALGUR(fi3,fiAC+PI05,VC,ALVC,-VA,ALVA,0,0,0,0,VA3A,VA3C);
             {�� �������� �����������}
   ALVA3A:=UGPI(fi3,VA3A);
   ALVA3C:=UG05(fiAC,VA3C);
   v3:=-VA3A;
   {PVBA:=ABS(PVBA);
   PVBC:=ABS(PVBC);}
                {����������� ������ �������� �.A3}
   PROXY(VA,ALVA,Abs(v3),ALVA3A,0,0,VB,ALVB);
                {������� ��������}
   Omega3:=-Abs(VA3C/AC);
    IF MEN(fiAC,ALVA3C) THEN
     Omega3:=-Omega3;
             {���������}
               {������� � �������� �����������}
   XYRFi(aax,aay,WA,BETWA);
   XYRFi(acx,acy,WC,BETWC);
                {���������� ������������ ����������� ���������}
   PWNA3C:=Sqr(Omega3)*AC;
   PFINA3C:=FIOGR(fiAC+Pi);
                {��������� ���������}
   PWKA3A:=ABS(2*Omega3*v3);
   PFIKA3A:=UG05(ALVA3A,Omega3);
   aNA3C:=PWNA3C;
   fiNA3C:=PFINA3C;
   aKA3A:=PWKA3A;
   fiKA3A:=PFIKA3A;
                {����������� ������������� ��������� }
   ALGUR(fi3,ALVA3C,WC,BETWC,PWNA3C,PFINA3C,-PWKA3A,PFIKA3A,
         -WA,BETWA,a3,WTA3C);
   FITA3C:=UGPI(ALVA3C,WTA3C);
   {PWTBA:=ABS(PWTBA);
   PWTBC:=ABS(PWTBC);}
                 {����������� ������� ��������� �.A3 }
   PROXY(WC,BETWC,PWNA3C,PFINA3C,Abs(WTA3C),FiTA3C,WA3,BETWA3);
   Epsi3:=-Abs(WTA3C/AC);
    IF MEN(fiAC,FITA3C) THEN
     Epsi3:=-Epsi3;

  End;   { Diada3v }

  Procedure Diada3vP(ax,ay, cx,cy, l2: double; Var fi3,l3, bpx,bpy: double);
      {��������� ������� ��������� Diada3v - ������ ����������� ���������}
          {������ 3-�� ���� - ���� �������������� (����������) ���� B }
          {����� bp - ��������� �������������� �� A �� ����������� l3 }
          {������� fi3=999 �������� �������������� ������             }

   {    A  o    l2     + }
   {         \ /     /   }
   {           \ /\/     }
   {           /   /     }
   {           \ /  Bp   }
   {         /           }
   {       / l3          }
   {  C  o               }

   { ��� l2<>0 ���� A ����������� ����� �� Fi3 ��� l2>0 � ������ ��� l2<0 }
  Var rab,AC,fiAC,psi: double;
  Begin   { Diada3v }
   AC:=Dist((ax-cx),(ay-cy));
   IF AC<ABS(l2) THEN
     BEGIN
      fi3:=999;
      exit;
     END;
   fiAC:=ARTFI((ax-cx),(ay-cy));
    IF l2=0 THEN
     BEGIN
      l3:=AC;
      fi3:=fiAC;
      bpx:=ax;
      bpy:=ay;
     END
    ELSE   {l2<>0}
     BEGIN
      PSI:=ArcSin(l2/AC);
      fi3:=fiAC-PSI;
      CROS90(cx,cy,fi3,ax,ay,bpx,bpy,l3,rab);
     END;
  End;   { Diada3vP }

  Procedure Diada4v(ax,ay,vax,vay,aax,aay, fi1,omega1,epsi1,
                    cx,cy,vcx,vcy,acx,acy, fi4,omega4,epsi4, l2,l3: double;
  Var l1,v1,a1, l4,v4,a4, bx,by,vbx,vby,abx,aby, apx,apy, cpx,cpy,
                    akBB1,fikBB1, akBB4,fikBB4: double);

  {����� 4-�� ���� - ���� ������������ (����������) ���� B           }
  {����� Ap � Cp - ��������� ��������������� �� ���� B �� l1 � �� l4 }
  {������ ������������ ������� l2 � l3  (������������ ������ �����
   (A Ap B) � (C Cp B). ��� ������������� ��������� l2 > 0, � l3 < 0 }
  {������� l1=999 �������� �������������� ������             }
  {akBB1 � akBB4 - ������ �������� ��������� ��������� �.B}
  {������������ �.B ����� 1 � �.B ����� 4 ��������������}
  {fikBB1 � fikBB4 - ���� ������� ���� ��������                     }

   {                                       }
   {             /\/       \/\             }
   {           /   /       \   \           }
   {           \ / \       / \ /           }
   {      l1 /    l2 \   /l3     \l4       }
   {       /           o           \       }
   {     *               B           *     }
   {   /  A                        C   \   }

  Var rab,
   FI,f2,f3,x0,y0,xb1,yb1,VA,ALVA,WA,BETWA,VB,ALVB,VC,ALVC,WC,BETWC: double;
   VB1,ALVB1,WB1,BETWB1,VB4,ALVB4,WB4,BETWB4,ALBA,ALBC:double;
   WKBB1,PFIKBB1,WKBB4,PFIKBB4,BETBA,BETBC,WB,BETWB : double;

  Begin  { Diada4v }
   FI:=ABS(FIOGR(fi1-fi4));
    IF (FI<=ERR) OR (ABS(fI-PI)<=ERR) THEN
     BEGIN
      l1:=999;
      Exit;
     END;
    IF L2<>0 THEN
     F2:=UG05(fi1,L2)
    ELSE
     F2:=fi1;
    IF L3<>0 THEN
     F3:=UG05(fi4,L3)
    ELSE
     F3:=fi4;
   XY(ax,ay,ABS(L2),F2+PI,X0,Y0);
   XY(cx,cy,ABS(L3),F3+PI,XB1,YB1);
   CROS(X0,Y0,fi1,XB1,YB1,fi4,bx,by);
    IF L2=0 THEN
     BEGIN
      apx:=bx;
      apy:=by;
     END
    ELSE
     BEGIN
{      PX0:=ax;
      PY0:=ay;}
      {XY(bx,by,ABS(L2),F2,apx,apy);}
      CROS90(ax,ay,fi1,bx,by,apx,apy,l1,rab);
     END;
    IF L3=0 THEN
     BEGIN
      cpx:=bx;
      cpy:=by;
     END
    ELSE
     BEGIN
     { PXB1:=PXC;
      PYB1:=PYC; }
     { XY(bx,by,ABS(L3),fi3,cpx,cpy); }
      CROS90(cx,cy,fi4,bx,by,cpx,cpy,l4,rab);
     END;
   {�������� � ���������}
        {��������}
          {������� � �������� ����������}
   XYRFi(vax,vay,VA,ALVA);
   XYRFi(aax,aay,WA,BETWA);
          {�������� � ��������� �.B, ��������� �� ������ l1}
   VWTXY(ax,ay,VA,ALVA,WA,BETWA,omega1,epsi1,bx,by,VB1,ALVB1,WB1,BETWB1);
  { VWTXY(PXA,PYA,PVA,PALVA,PWA,PBETWA,POMA,PEPA,PXB,PYB,PX0,PSI,PY0,PCO);}
          {������� � �������� ����������}
   XYRFi(vcx,vcy,VC,ALVC);
   XYRFi(acx,acy,WC,BETWC);
          {�������� � ��������� �.B, ��������� �� ������ l4}
   VWTXY(cx,cy,VC,ALVC,WC,BETWC,omega4,epsi4,bx,by,VB4,ALVB4,WB4,BETWB4);
   {VWTXY(PXC,PYC,PVC,PALVC,PWC,PBETWC,POMC,PEPC,PXB,PYB,PXB1,PSI2,PYB1,PCO2);
   }      {������������� �������� ����� B (������������ B1 � B4) }
   ALGUR(fi1,fi4,-VB1,ALVB1,VB4,ALVB4,0,0,0,0,v1,v4);
          {�� �������� �����������}
   ALBA:=UGPI(fi1,v1);
   ALBC:=UGPI(fi4,v4);
             {�������� ������������ ������� B }
   PROXY(VB1,ALVB1,Abs(v1),ALBA,0,0,VB,ALVB);
   vbx:=VB*cos(ALVB); vby:=VB*sin(ALVB);

           {���������}
                {��������� ���������}
   WKBB1:=ABS(2*omega1*v1);
   PFIKBB1:=UG05(ALBA,omega1);
   WKBB4:=ABS(2*omega4*v4);
   PFIKBB4:=UG05(ALBC,omega4);
   akBB1:= WKBB1;
   FiKBB1:=PFIKBB1;
   akBB4:= WKBB4;
   FiKBB4:=PFIKBB4;
                 {������������� ���������}
   ALGUR(fi1,fi4,-WB1,BETWB1,WB4,BETWB4,-WKBB1,PFIKBB1,
        WKBB4,PFIKBB4,a1,a4);
   BETBA:=UGPI(fi1,a1);
   BETBC:=UGPI(fi4,a4);
                 {������ ��������� ������������ ������� B }
   PROXY(WB1,BETWB1,WKBB1,PFIKBB1,Abs(a1),BETBA,WB,BETWB);
   abx:= WB*cos(BETWB); aby:= WB*sin(BETWB);

  End; { Diada4v }

  Procedure Diada4vP(ax,ay, fi1, cx,cy, fi4, l2,l3: double;
            Var l1,l4, bx,by, apx,apy, cpx,cpy: double);
  {����������� ������� ��������� Diada4v - ������ ��� ���������}
  {����� 4-�� ���� - ���� ������������ (����������) ���� B           }
  {����� Ap � Cp - ��������� ��������������� �� ���� B �� l1 � �� l4 }
  {������ ������������ ������� l2 � l3  (������������ ������ �����
   (A Ap B) � (C Cp B). ��� ������������� ��������� l2 > 0, � l3 < 0 }
  {������� l1=999 �������� �������������� ������             }

   {                                       }
   {             /\/       \/\             }
   {           /   /       \   \           }
   {           \ / \       / \ /           }
   {      l1 /    l2 \   /l3     \l4       }
   {       /           o           \       }
   {     *               B           *     }
   {   /  A                        C   \   }

  Var rab,FI,f2,f3,x0,y0,xb1,yb1: double;

  Begin  { Diada4vP }
   FI:=ABS(FIOGR(fi1-fi4));
    IF (FI<=ERR) OR (ABS(fI-PI)<=ERR) THEN
     BEGIN
      l1:=999;
      Exit;
     END;
    IF L2<>0 THEN
     F2:=UG05(fi1,L2)
    ELSE
     F2:=fi1;
    IF L3<>0 THEN
     F3:=UG05(fi4,L3)
    ELSE
     F3:=fi4;
   XY(ax,ay,ABS(L2),F2+PI,X0,Y0);
   XY(cx,cy,ABS(L3),F3+PI,XB1,YB1);
   CROS(X0,Y0,fi1,XB1,YB1,fi4,bx,by);
    IF L2=0 THEN
     BEGIN
      apx:=bx;
      apy:=by;
     END
    ELSE
     BEGIN
{      PX0:=ax;
      PY0:=ay;}
      {XY(bx,by,ABS(L2),F2,apx,apy);}
      CROS90(ax,ay,fi1,bx,by,apx,apy,l1,rab);
     END;
    IF L3=0 THEN
     BEGIN
      cpx:=bx;
      cpy:=by;
     END
    ELSE
     BEGIN
     { PXB1:=PXC;
      PYB1:=PYC; }
     { XY(bx,by,ABS(L3),fi3,cpx,cpy); }
      CROS90(cx,cy,fi4,bx,by,cpx,cpy,l4,rab);
     END;

  End; { Diada4vP }

 Procedure Diada5v(ax,ay,vax,vay,aax,aay, cx,cy,vcx,vcy,acx,acy,
                   l2,fi34, fi4,omega4,epsi4: double;
           Var fil2,fil3, lCp,vCp,aCp, lBp,vBp,aBp, xCp,yCp, xBp,yBp,
               akA3A4,fikA3A4, akA3A2,fikA3A2: double);
  { ����� 5-�� ���� - ������ ���� ������������ (�������) ���� A          }

  {         A  o    l2     + }{Bp - ��������� ������������� �� A �� l3   }
  {              \ /     /   }{Cp - ����� ����������� ������������ l3 � 4}
  {                \ /\/     }{ C - �������� ����� �� ������������ 4     }
  {                /   /     }{fi4, omega4, epsi4 - ������, ������� ��������}
  {          l3    \ /  Bp   }{ � ������� ��������� ������������ 4       }
  {            \ /           }{fi34 - ������ ������������ l3 ������������}
  {   C      __/_     4      }{������������ 4. � ��������� �� ����� - ����.}
  { -*------|____|-------> + }{lCp - ��������� ������ Cp ������������ �.C}
  {             Cp           }{lBp - ��������� ������ Bp ������������ Cp }
                              {� lCp � lBp ����� ���� ��������������     }
  {����� ������� ����� l2 ����� ���� � ����������� �� ����, � �����      }
  {������� ������������ Bp ����� A. � ��������� �� ����� - l2 > 0        }
  {���� fil2 - ���� ������� A-Bp, ���� fil3 - ���� ������� Cp-Bp         }
  {akA3A4 � akA3A2 - ������ �������� ��������� ��������� �.A , ��������� ��}
  {������ l3, ������������ �.A ����� 4 � ������������ ���� A ��������������}
  {fikA3A4 � fikA3A2 - ���� ������� ���� ��������                        }

  var rab: double;
  PVA,PALVA,PWA,PBETWA,PVC,PALVC,PWC,PBETWC,PVA4,PALVA4,
  VBA,VBC,WBA,WBC,PWA4,PBETWA4:double;
  PALBA,PALBC,PVA3,PWKA3A2,PBETKBA,PWKA3A4,PBETKBC,PBETBA,PBETBC:double;
  Begin   { Diada5v }
   FIl3:=FI4+fi34;
    IF L2<>0 THEN
     Fil2:=UG05(FIl3,-L2)
    ELSE
     Fil2:=FIl3;                     {��������� Bp}
   XY(ax,ay,ABS(L2),Fil2,xBp,yBp);
                                     {��������� Cp}
   CROS(xBp,yBp,Fil3,cx,cy,FI4,xCp,yCp);
                  {�������� Bp ������������ Cp, � Cp ������������ C}
   CROS90(xCp,yCp,Fil3,xBp,yBp,rab,rab,lBp,rab);
   CROS90(cx,cy,Fi4,xCp,yCp,rab,rab,lCp,rab);

   {�������� � ���������}
     {��������}
          {������� � �������� ������� ���������}
   XYRFi(vax,vay,PVA,PALVA);
   XYRFi(aax,aay,PWA,PBETWA);
   XYRFi(vcx,vcy,PVC,PALVC);
   XYRFi(acx,acy,PWC,PBETWC);
         {���������� �������� � ��������� �.A4 (������������� ����� 4) }
   VWTXY(cx,cy,PVC,PALVC,PWC,PBETWC,omega4,epsi4,ax,ay,
          PVA4,PALVA4,PWA4,PBETWA4);
        {������������� �������� �.A3 (������������ ����� A2 � A4) }
  ALGUR(FIl3,Fi4,PVA4,PALVA4,-PVA,PALVA,0,0,0,0,VBA,VBC);
           {�� �������� �����������}

   PALBA:=UGPI(Fil3,VBA);
   PALBC:=UGPI(Fi4,VBC);
   vBp:=-VBA;
   vCp:=VBC;

        {������ ���������� �������� �.A3}
  { PROXY(PVA4,PALVA4,Abs(vCp),PALBC,0,0,vA3,fivA3);}

     {���������}
               {��������� ���������}
   PWKA3A2:=ABS(2*vBp*Omega4);
   PBETKBA:=UG05(PALBA,Omega4);
   PWKA3A4:=ABS(2*vCp*Omega4);
   PBETKBC:=UG05(PALBC,Omega4);
   akA3A2:=PWKA3A2;
   fikA3A2:=PBETKBA;
   akA3A4:=PWKA3A4;
   fikA3A4:=PBETKBC;
       {������������� ��������� �.A3 (������������ ����� A2 � A4) }
   ALGUR(Fil3,Fi4,PWA4,PBETWA4,-PWA,PBETWA,PWKA3A4,PBETKBC,
        -PWKA3A2,PBETKBA,WBA,WBC);
                    {�� �������� �����������}
   PBETBA:=UGPI(Fil3,WBA);
   PBETBC:=UGPI(Fi4,WBC);
   aBp:=-WBA;
   aCp:=WBC;
         {������ ������� ��������� �.A3 }
   {PROXY(PWA4,PBETWA4,PWKA3A4,PBETKBC,aCp,PBETBC,aA3,fiaA3);}
   (*
   VWTXY(PXA,PYA,PVB,PALVB,PWB,PBETWB,POMC,PEPC,PXB,PYB,PVB,PALVB,PWB,PBETWB);
   *)
  End;  { Diada5v }

  Procedure Diada5vP(ax,ay, cx,cy, l2,fi34, fi4: double;
           Var fil2,fil3, lCp,lBp, xCp,yCp, xBp,yBp : double);
  {����������� ������� ��������� Diada5v ��� ���������� ������ ��������� }

  {         A  o    l2     + }{Bp - ��������� ������������� �� A �� l3}
  {              \ /     /   }{Cp - ����� ����������� ������������ l3 � 4}
  {                \ /\/     }{ C - �������� ����� �� ������������ 4 }
  {                /   /     }{fi4 - ������ ������������ 4}
  {          l3    \ /  Bp   }
  {            \ /           }{fi34 - ������ ������������ l3 ������������}
  {   C      __/_     4      }{������������ 4. � ��������� �� ����� - ����.}
  { -*------|____|-------> + }{lCp - ��������� ������ Cp ������������ �.C}
  {             Cp           }{lBp - ��������� ������ Bp ������������ Cp}
                              {� lCp � lBp ����� ���� ��������������}
  {����� ������� ����� l2 ����� ���� � ����������� �� ����, � �����}
  {������� ������������ Bp ����� A. � ��������� �� ����� - l2 > 0 }
  {���� fil2 - ���� ������� A-Bp, ���� fil3 - ���� ������� Cp-Bp }

  var rab: double;
  Begin   { Diada5vP }
   FIl3:=FI4+fi34;
    IF L2<>0 THEN
     Fil2:=UG05(FIl3,-L2)
    ELSE
     Fil2:=FIl3;                     {��������� Bp}
   XY(ax,ay,ABS(L2),Fil2,xBp,yBp);
                                     {��������� Cp}
   CROS(xBp,yBp,Fil3,cx,cy,FI4,xCp,yCp);
                  {�������� Bp ������������ Cp, � Cp ������������ C}
   CROS90(xCp,yCp,Fil3,xBp,yBp,rab,rab,lBp,rab);
   CROS90(cx,cy,Fi4,xCp,yCp,rab,rab,lCp,rab);

  End;  { Diada5vP }

End.  { Unit fDiada }
