unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,mech, ExtCtrls, StdCtrls,  ValEdit, Graphic, Grids, ComCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    ValueListEditor1: TValueListEditor;
    frameGraph: TGroupBox;
    frameMech: TGroupBox;
    Image1: TImage;
    Image2: TImage;

    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    ImageBig: TImage;
    TimeLine: TTrackBar;
    Back: TButton;
    Forward: TButton;
    TxtAngle: TEdit;
    Stop: TButton;
    Timer2: TTimer;
    procedure formCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure onPaint(Sender: TObject);

    procedure onImageBigDbClick(Sender: TObject);
    procedure Image2DbClick(Sender: TObject);
    procedure Image3DblClick(Sender: TObject);
    procedure TimeLineChange(Sender: TObject);
    procedure BackClick(Sender: TObject);
    procedure StopClick(Sender: TObject);
    procedure ForwardClick(Sender: TObject);
    procedure Image4DbClick(Sender: TObject);
    procedure Image5DbClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure onValueChange(Sender: TObject);
    procedure ValueListEditor1GetEditText(Sender: TObject; ACol,
      ARow: Integer; var Value: String);
  private
  {mechanism: Tmechanism;}
  graphEnergy:TgraphicView;
  graphForce:TgraphicView;
  graphMoment:TgraphicView;
  graphJob:TgraphicView;
  angle:integer;
  rectFull,rectTmp:Trect;
  bigView:boolean;
  state:integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.formCreate(Sender: TObject);
var
ss:string;
begin
  bigView:=false;
  mechanism.mechGraph.Init(Image1.canvas,Image1.Width,Image1.Height);
  mechanism.InitParam;
  mechanism.InitMech;

  mechanism.Result;        
  graphEnergy.Init(Image2.Canvas);
  graphEnergy.AddGraphByData( @mechanism.T2,'T2',clRed,1);
  graphEnergy.AddGraphByData( @mechanism.T3,'T3',clNavy,1);
  graphEnergy.AddGraphByData( @mechanism.T4,'T4',clOlive,1);
  graphEnergy.AddGraphByData( @mechanism.T5,'T5',clMaroon,1);
  graphEnergy.AddGraphByData( @mechanism.Tsum2,'sum',clFuchsia,1);

  graphEnergy.SetSize(Image2.Width,Image2.Height);
  graphEnergy.SetPos(0,0);

  graphForce.Init(Image3.Canvas);
  graphForce.AddGraphByData(@mechanism.mfs,'F',clRed,1);
  graphForce.AddGraphByData(@mechanism.vex,'V',clBlue,800);

  graphForce.SetSize(Image3.Width,Image3.Height);
  graphForce.SetPos(0,0);

  graphMoment.Init(Image4.Canvas);
  graphMoment.AddGraphByFn(mech_mprd,'mprd',clRed,1);
  graphMoment.AddGraphByData(@mechanism.mpr,'Mpr',clTeal,1);
  graphMoment.AddGraphByFn(mech_SumPrivMoment,'Msp',clBlue,1);
  graphMoment.SetSize(Image4.Width,Image4.Height);
  graphMoment.SetPos(0,0);

  graphJob.Init(Image5.canvas);
  graphJob.AddGraphByData(@mechanism.asum,'Asum',clTeal,1);
  graphJob.AddGraphByFn(mech_Ksum,'Tkin',clGreen,1);
  graphJob.SetSize(Image5.Width,Image5.Height);
  graphJob.SetPos(0,0);

{  ValueListEditor1.Showing}
  Str(mechanism.AB.l:5:3,ss); ValueListEditor1.InsertRow('AB, �',ss,true);
  Str(mechanism.BC.l:5:3,ss); ValueListEditor1.InsertRow('BC, �',ss,true);
  Str(mechanism.CD.l:5:3,ss); ValueListEditor1.InsertRow('CD, �',ss,true);
  Str(mechanism.DE.l:5:3,ss);  ValueListEditor1.InsertRow('DE, �',ss,true);
  Str(mechanism.fs:7:3,ss);   ValueListEditor1.InsertRow('F, �',ss,true);
  {                  OutTextXY(10,80,'Fi(���. ����):');  }
  {Str(mechanism.imin:3,ss);   ValueListEditor1.InsertRow('���',ss,true);
  Str(mechanism.imax:3,ss);   ValueListEditor1.InsertRow('���',ss,true); }
  Str(mechanism.m2:4:0,ss);  ValueListEditor1.InsertRow('m2, ��',ss,true);
  Str(mechanism.m3:4:0,ss);  ValueListEditor1.InsertRow('m3, ��',ss,true);

  Str(mechanism.m4:4:0,ss);  ValueListEditor1.InsertRow('m4, ��',ss,true);
  Str(mechanism.m5:4:0,ss);  ValueListEditor1.InsertRow('m5, ��',ss,true);
  Str(mechanism.deltai:2:3,ss); ValueListEditor1.InsertRow('Delta','1/'+ss,true);
  {Str(Imaxmas:4:0,ss); ValueListEditor1.InsertRow('BC, �',ss,true);OutTextXY(10,170,'�������'+ss+' ��*�*�');}
  {ValueListEditor1.InsertRow("AB",ss,true);}
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
tt:string[3];
begin
  if angle>360 then angle:=0;
  if angle<0 then angle:=360;
  if state=1 then angle:=angle+1;
  if state=2 then angle:=angle-1;

end;

procedure TForm1.onPaint(Sender: TObject);
begin
  graphEnergy.DrawSystem;
  graphForce.DrawSystem;
  graphMoment.DrawSystem;
  {Chart1.GetASeries.DrawBetweenPoints:=true; }
end;
procedure TForm1.onImageBigDbClick(Sender: TObject);
begin
   graphEnergy.SetSize(Image2.Width,Image2.Height);
   Image2.Visible:=true;
   Image3.Visible:=true;
   Image4.Visible:=true;
   Image5.Visible:=true;

   graphEnergy.output:=(Image2.Canvas);
   graphEnergy.SetSize(Image2.Width,Image2.Height);

   graphForce.output:=Image3.Canvas;
   graphForce.SetSize(Image3.Width,Image3.Height);

   graphMoment.output:=Image4.canvas;
   graphMoment.SetSize(Image4.Width,Image4.Height);

   graphJob.output:=Image5.canvas;
   graphJob.SetSize(Image5.Width,Image5.Height);

   ImageBig.Visible:=false;
end;
procedure TForm1.Image2DbClick(Sender: TObject);
begin
  Image2.Visible:=false;
  Image3.Visible:=false;
  Image4.Visible:=false;
  Image5.Visible:=false; 
  //ImageBig.Canvas.Brush.Color:=clWhite;
  ImageBig.Canvas.FillRect(ImageBig.ClientRect);
  graphEnergy.output:=(ImageBig.Canvas);
  graphEnergy.SetSize(ImageBig.Width,ImageBig.Height);
  ImageBig.Visible:=true;
  onPaint(nil);
end;

procedure TForm1.Image3DblClick(Sender: TObject);
begin
  Image2.Visible:=false;
  Image3.Visible:=false;
  Image4.Visible:=false;
  Image5.Visible:=false;

  ImageBig.Canvas.FillRect(ImageBig.ClientRect);
  graphForce.output:=(ImageBig.Canvas);
  graphForce.SetSize(ImageBig.Width,ImageBig.Height);
  ImageBig.Visible:=true;
  onPaint(nil);
end;

procedure TForm1.TimeLineChange(Sender: TObject);
begin
  angle:=TimeLine.Position
end;

procedure TForm1.BackClick(Sender: TObject);
begin
  state:=2;
end;
procedure TForm1.StopClick(Sender: TObject);
begin
  state:=0;
end;

procedure TForm1.ForwardClick(Sender: TObject);
begin
  state:=1;
end;
procedure TForm1.Image4DbClick(Sender: TObject);
begin
  Image2.Visible:=false;
  Image3.Visible:=false;
  Image4.Visible:=false;
  Image5.Visible:=false;

  ImageBig.Canvas.FillRect(ImageBig.ClientRect);
  graphMoment.output:=(ImageBig.Canvas);
  graphMoment.SetSize(ImageBig.Width,ImageBig.Height);
  ImageBig.Visible:=true;
  onPaint(nil);
end;

procedure TForm1.Image5DbClick(Sender: TObject);
begin
  Image2.Visible:=false;
  Image3.Visible:=false;
  Image4.Visible:=false;
  Image5.Visible:=false;

  ImageBig.Canvas.FillRect(ImageBig.ClientRect);
  graphJob.output:=(ImageBig.Canvas);
  graphJob.SetSize(ImageBig.Width,ImageBig.Height);
  ImageBig.Visible:=true;
  onPaint(nil);
end;

procedure TForm1.Timer2Timer(Sender: TObject);
var
tt:string[3];
begin
canvas.Rectangle(0,250,168,400);
  Image1.Canvas.Rectangle(Image1.ClientRect);
  mechanism.DrawMech(angle);
  graphEnergy.DrawValue(angle);
  graphForce.DrawValue(angle);
  graphMoment.DrawValue(angle);
  graphJob.DrawValue(angle);
  TimeLine.Position:=angle;
  Str(angle,tt);
  txtAngle.Text:=tt; 
end;

procedure TForm1.onValueChange(Sender: TObject);
var
i:integer;
begin
   i:=2;
end;

procedure TForm1.ValueListEditor1GetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: String);
  var
i:integer;
begin
   i:=2;
end;
end.
