unit fnmath;
{
���������� ������ "�������"
��������� �����������:
�������� � ���������: ��������, �����������������(���������),
��������������(���������)
������� �� ������
}
interface
uses sysutils;
const
  step=0.01;
type
  Preal=^real;
  TValueFn=function(x:real):real;
  TArray=object
    data:Preal;
    size:integer;
    constructor Init(newsize:integer);
    function GetValue(i:integer):real;
    procedure SetValue(value:real;i:integer);
    destructor Free;
  end;
  {������� ����� ��� ���� �������� � ���������}
  TBaseFunction=class
    function value(x:real):real;virtual;abstract; {�������� ������� � �����}
    function integral(a,b:real):real;virtual; {�������� ������� �� a �� b}
    function diff(x:real):real;virtual;
  end;
  PBaseFn=^TBaseFunction;
  {�������-�����}
  TSumFunction=class(TBaseFunction)
    num:integer;
    fn:array[0..5] of PBaseFn;
    Constructor Init(a,b:PbaseFn);
    function value(x:real):real;override;
  end;
  {�������- �������� ���������� �������}
  TLinearCombine=class(TBaseFunction)
    num:integer;
    f:array[0..10] of PBaseFn;
    c:array[0..10] of real; {������ �������������}
    procedure AddFn(fn:PBaseFn;coeff:real);
    function value(x:real):real;override;
  end;
  {�������-������}
  TArrayFunction=class(TBaseFunction)
    {data }
    function value(x:real):real;override;
  end;
  {�������-���������}
  TRefFunction=class(TBaseFunction)
    fn:TValueFn;
    constructor Init(_fn:TValueFn);
    function value(x:real):real; override;
  end;
  {�������-��������}
  TIntegralFunction=class(TBaseFunction)
    fn:PBaseFn;
    constructor Init(source:PBaseFn);
    function value(x:real):real;override;
  end;

implementation

{==========================================}
{������������ ������}
{==========================================}
constructor TArray.Init(newsize:integer);
begin
  size:=newsize;
  data:=allocMem(size);
end;
function TArray.GetValue(i:integer):real;
var
p:Preal;
begin
  if data=nil then GetValue:=0
  else begin
    p:=data;
    inc(p,i);
    GetValue:=p^;
  end;
end;
procedure TArray.SetValue(value:real;i:integer);
var
p:Preal;
begin
  if i<size then
  begin 
    p:=data;
    inc(p,i);
    p^:=value;
  end;
end;
destructor TArray.Free;
begin
  if data<>nil then freemem(data);
end;
{==========================================}
{������� ����� ��� �������}
{==========================================}
function TBaseFunction.integral(a,b:real):real;
var
sum:real;
x:real;
begin
  x:=a;
  while x<=b do begin
    sum:=sum+value(x);
    x:=x+step;
  end;
end;
function TBaseFunction.diff(x:real):real;
begin
  diff:=(value(x+step)-value(x-step))/(step+step);
end;
{=========================================}
{�������-�����}
{=========================================}
constructor TSumFunction.Init(a,b:PbaseFn);
begin
  fn[0]:=a;
  fn[1]:=b;
  num:=2;
end;
function TSumFunction.value(x:real):real;
var
sum:real;
i:integer;
begin
  for i:=0 to num do begin
    sum:=sum+fn[i].value(x);
  end;
  value:=sum;
end;
{=========================================}
{�������� ���������� �������}
{=========================================}
procedure TLinearCombine.AddFn(fn:PBaseFn;coeff:real);
begin
  if num<10 then begin
    f[num]:=fn;
    c[num]:=coeff;
    inc(num);
  end;
end;
function TLinearCombine.value(x:real):real;
var
sum:real;
i:integer;
begin
  for i:=0 to num do begin
    sum:=sum+f[i].value(x)*c[i];
  end;
  value:=sum;
end;
{=========================================}
{�������-������ }
{=========================================}
function TArrayFunction.value(x:real):real;
begin
  value:=0;
end;
{=========================================}
{�������-��������}
{=========================================}
constructor TIntegralFunction.Init(source:PBaseFn);
begin
  fn:=source;
end;
function TIntegralFunction.value(x:real):real;
begin
  if fn<>nil then value:=(fn^).value(x);
end;
{=========================================}
{�������-���������}
{=========================================}
constructor TRefFunction.Init(_fn:TValueFn);
begin
  fn:=_fn;
end;
function TRefFunction.value(x:real):real;
begin
  value:=fn(x);
end;
end.
