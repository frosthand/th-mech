object Form1: TForm1
  Left = 313
  Top = 146
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnPaint = onPaint
  PixelsPerInch = 96
  TextHeight = 13
  object frameGraph: TGroupBox
    Left = 272
    Top = 0
    Width = 529
    Height = 481
    Caption = #1043#1088#1072#1092#1080#1082#1080
    TabOrder = 1
    object ImageBig: TImage
      Left = 8
      Top = 16
      Width = 513
      Height = 449
      Visible = False
      OnDblClick = onImageBigDbClick
    end
    object Image2: TImage
      Left = 8
      Top = 16
      Width = 513
      Height = 113
      Stretch = True
      OnDblClick = Image2DbClick
    end
    object Image3: TImage
      Left = 8
      Top = 128
      Width = 513
      Height = 113
      OnDblClick = Image3DblClick
    end
    object Image4: TImage
      Left = 8
      Top = 240
      Width = 513
      Height = 113
      OnDblClick = Image4DbClick
    end
    object Image5: TImage
      Left = 8
      Top = 352
      Width = 513
      Height = 113
      OnDblClick = Image5DbClick
    end
  end
  object ValueListEditor1: TValueListEditor
    Left = 0
    Top = 8
    Width = 265
    Height = 225
    Ctl3D = True
    FixedCols = 1
    ParentCtl3D = False
    TabOrder = 0
    OnGetEditText = ValueListEditor1GetEditText
    OnStringsChange = onValueChange
    ColWidths = (
      150
      109)
  end
  object frameMech: TGroupBox
    Left = 0
    Top = 240
    Width = 265
    Height = 305
    Caption = #1052#1077#1093#1072#1085#1080#1079#1084
    ParentBackground = False
    TabOrder = 2
    object Image1: TImage
      Left = 8
      Top = 16
      Width = 249
      Height = 281
    end
  end
  object TimeLine: TTrackBar
    Left = 272
    Top = 488
    Width = 521
    Height = 25
    Max = 359
    TabOrder = 3
    ThumbLength = 18
    OnChange = TimeLineChange
  end
  object Back: TButton
    Left = 480
    Top = 520
    Width = 75
    Height = 25
    Caption = #1053#1072#1079#1072#1076
    TabOrder = 4
    OnClick = BackClick
  end
  object Forward: TButton
    Left = 640
    Top = 520
    Width = 75
    Height = 25
    Caption = #1042#1087#1077#1088#1077#1076
    TabOrder = 5
    OnClick = ForwardClick
  end
  object TxtAngle: TEdit
    Left = 384
    Top = 520
    Width = 89
    Height = 21
    ReadOnly = True
    TabOrder = 6
    Text = 'TxtAngle'
  end
  object Stop: TButton
    Left = 560
    Top = 520
    Width = 75
    Height = 25
    Caption = #1057#1090#1086#1087
    TabOrder = 7
    OnClick = StopClick
  end
  object Timer1: TTimer
    Interval = 10
    OnTimer = Timer1Timer
    Left = 136
    Top = 48
  end
  object Timer2: TTimer
    Interval = 50
    OnTimer = Timer2Timer
    Left = 120
    Top = 168
  end
end
